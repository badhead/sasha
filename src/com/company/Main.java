package com.company;

public class Main {

    public static void main(String[] args) {
        double lengthMeter = 100;
        double weightKG = 100;
        double conversionMile =0.000621371;
        double conversionYard =1.09361;
        double conversionFt =0.3048;
        double conversionOz =35.274;
        double conversionLb = 2.20462;


        System.out.println("Meters in miles=" + lengthMeter*conversionMile);
        System.out.println("Meters in yards=" + lengthMeter*conversionYard);
        System.out.println("Meters in ft=" + lengthMeter*conversionFt);
        System.out.println("Kg in Oz=" + weightKG*conversionOz);
        System.out.println("Kg in lb=" + weightKG*conversionLb);


    }
}
